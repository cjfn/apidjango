from django.db import models


class UserRol(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    active = models.BooleanField(default=True)

    def __str__(self):
        return (self.name, self.description)

class User(models.Model):
    name = models.CharField("Name", max_length=240)
    password = models.CharField("Password", max_length=240)
    email = models.EmailField()
    created = models.DateField(auto_now_add=True)
    UserRol = models.ForeignKey(UserRol, on_delete=models.CASCADE)

    def __str__(self):
        return (self.name, self.password)
      
    class Meta:
        unique_together = ['name', 'email']



