from django.urls import include, path
from .views import UserCreate, UserList, UserDetail, UserUpdate, UserDelete


urlpatterns = [
    path('create/', UserCreate.as_view(), name='create-users'),
    path('', UserList.as_view()),
    path('<int:pk>/', UserDetail.as_view(), name='retrieve-users'),
    path('update/<int:pk>/', UserUpdate.as_view(), name='update-users'),
    path('delete/<int:pk>/', UserDelete.as_view(), name='delete-users')
]