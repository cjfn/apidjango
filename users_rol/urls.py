from django.urls import include, path
from .views import UserRolCreate, UserRolList, UserRolDetail, UserRolUpdate, UserRolDelete


urlpatterns = [
    path('create/', UserRolCreate.as_view(), name='create-users_rol'),
    path('', UserRolList.as_view()),
    path('<int:pk>/', UserRolDetail.as_view(), name='retrieve-users_rol'),
    path('update/<int:pk>/', UserRolUpdate.as_view(), name='update-users_rol'),
    path('delete/<int:pk>/', UserRolDelete.as_view(), name='delete-users_rol')
]