from django.db import models


class UserRol(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    active = models.BooleanField(default=True)

    def __str__(self):
        return (self.name, self.description)