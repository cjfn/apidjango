from rest_framework import serializers
from .models import UserRol

class UserRolSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserRol
        fields = ['pk', 'name', 'description', 'active']
