from django.shortcuts import render
from .models import UserRol
from rest_framework import generics
from .serializers import UserRolSerializer


class UserRolCreate(generics.CreateAPIView):
    # API endpoint that allows creation of a new UserRol
    queryset = UserRol.objects.all(),
    serializer_class = UserRolSerializer


class UserRolList(generics.ListAPIView):
    # API endpoint that allows UserRol to be viewed.
    queryset = UserRol.objects.all()
    serializer_class = UserRolSerializer

class UserRolDetail(generics.RetrieveAPIView):
    # API endpoint that returns a single UserRol by pk.
    queryset = UserRol.objects.all()
    serializer_class = UserRolSerializer

class UserRolUpdate(generics.RetrieveUpdateAPIView):
    # API endpoint that allows a UserRol record to be updated.
    queryset = UserRol.objects.all()
    serializer_class = UserRolSerializer
    
class UserRolDelete(generics.RetrieveDestroyAPIView):
    # API endpoint that allows a UserRol record to be deleted.
    queryset = UserRol.objects.all()
    serializer_class = UserRolSerializer

# Create your views here.
